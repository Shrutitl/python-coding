
# Return a float representing the current system-wide CPU utilization as a percentage
import psutil


cpu_stats = psutil.cpu_percent(60)

print(cpu_stats)
