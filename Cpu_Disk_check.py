import shutil
import psutil


def check_disk_usage(disk):
    """Verifies that there's enough free space on disk"""
    du = shutil.disk_usage(disk)
    print (du)
    free = du.free / du.total * 100
    return free > 50


def check_cpu_usage():
    """Verifies that there's enough unused CPU"""
    usage = psutil.cpu_percent(1)
    return usage < 75


if not check_disk_usage("/") or not check_cpu_usage():
    print("Error!")
else:
    x = check_disk_usage("/")
    print("Everything is OK!", psutil.cpu_percent(1))
